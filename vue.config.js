module.exports = {
    "transpileDependencies": [
        "vuetify"
    ],
    publicPath: '/',
    // publicPath: process.env.NODE_ENV === 'production'
    //     ? '/'
    //     : '/',
    productionSourceMap: false,
    // outputDir: 'D:\\ENVIRONMENT\\nginx-1.16.0\\html\\web', // 开发调试用
    devServer: {
        proxy: {
            '/api': {
                target: 'http://localhost:9090/simple',
                changeOrigin: true,
                pathRewrite: {
                    '^/api': ''
                }
            },
            '/geo': {
                target: 'http://localhost:19099',
                changeOrigin: true,
                pathRewrite: {
                    '^/geo': '/geo'
                }
            },
        }
    }
}
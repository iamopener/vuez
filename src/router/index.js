import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        meta: {
            title: '首页'
        },
        component: () => import('../views/Home')
    }, {
        path: '/about',
        name: 'About',
        meta: {
            title: '其他/测试/关于'
        },
        component: () => import('../views/test/Father.vue')
    }, {
        path: '/lnr',
        name: 'LNR',
        meta: {
            title: '子路由'
        },
        component: () => import('../views/children-router-view/LNR.vue'),
        children: [
            {
                path: 'a',
                name: 'A',
                meta: {
                    title: '子路由A'
                },
                component: () => import('../views/children-router-view/A.vue'),
            },
            {
                path: 'b',
                name: 'B',
                meta: {
                    title: '子路由B'
                },
                component: () => import('../views/children-router-view/B.vue'),
            }
        ]
    }, {
        path: '/docx',
        name: 'Docx',
        meta: {
            title: 'word文件生成'
        },
        component: () => import('../views/docx/Docx.vue')
    }, {
        path: '/md',
        name: 'Markdown',
        meta: {
            title: 'Markdown'
        },
        component: () => import('../views/markdown/Markdown.vue')
    }, {
        path: '/tm',
        name: 'TweenMax',
        meta: {
            title: 'TweenMax.js'
        },
        component: () => import('../views/tweenmax/TweenMax.vue')
    }, {
        path: '/anime',
        name: 'Anime',
        meta: {
            title: 'Anime.js'
        },
        component: () => import('../views/anime/Anime.vue')
    }, {
        path: '/crossPagesSelection',
        name: 'CrossPagesSelection',
        meta: {
            title: 'Element-UI 跨页多选'
        },
        component: () => import('../views/element-ui/CrossPagesSelection.vue')
    }, {
        path: '/multiPagesSelection',
        name: 'MultiPagesSelection',
        meta: {
            title: 'vuetify 跨页多选'
        },
        component: () => import('../views/vuetify/MultiPagesSelection.vue')
    }, {
        path: '/calendar',
        name: 'Calendar',
        meta: {
            title: '日历'
        },
        component: () => import('../views/calendar/Calendar')
    }, {
        path: '/ws',
        name: 'WebSocket',
        meta: {
            title: 'WebSocket'
        },
        component: () => import('../views/web-socket/WebSocket')
    }, {
        path: '/pages',
        name: 'Pages',
        meta: {
            title: 'DIY'
        },
        component: () => import('../views/pages/Pages')
    }, {
        path: '/neumorphism',
        name: 'Neumorphism',
        meta: {
            title: '新拟态'
        },
        component: () => import('../views/neumorphism/Neumorphism')
    }, {
        path: '/slot',
        name: 'Outer',
        meta: {
            title: 'vue slot'
        },
        component: () => import('../views/slot/SlotMain')
    }, {
        path: '/buttons',
        name: 'Buttons',
        meta: {
            title: '按钮'
        },
        component: () => import('../views/button/Buttons')
    }, {
        path: '/vueModel',
        name: 'VueModel',
        meta: {
            title: 'vue v-model 自定义组件'
        },
        component: () => import('../views/model/VueModel')
    }, {
        path: '/leonSans',
        name: 'LeonSans',
        meta: {
            title: 'LeonSans.js'
        },
        component: () => import('../views/leon-sans/LeonSans')
    }, {
        path: '/ol',
        name: 'OpenLayers',
        meta: {
            title: 'OpenLayers'
        },
        component: () => import('../views/openLayers/OpenLayers')
    }, {
        path: '/tif',
        name: 'Tif',
        meta: {
            title: 'Tif'
        },
        component: () => import('../views/tif/Tif')
    },
]

const router = new VueRouter({
    mode: 'history',
    // base: process.env.BASE_URL,
    base: 'vuez',
    routes
})

export default router

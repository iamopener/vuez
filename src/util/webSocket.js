function initWebSocket(webSocketUrl, funOnMsg) {
    let webSocket = new WebSocket(webSocketUrl);
    webSocket.onopen = () => console.log("WebSocket连接成功");
    webSocket.onmessage = funOnMsg;
    webSocket.onerror = e => console.log("WebSocket错误", e);
    webSocket.onclose = e => console.log('WebSocket连接关闭', e);
    return webSocket;
}